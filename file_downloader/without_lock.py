import requests
from concurrent.futures import ThreadPoolExecutor
import time


url = "https://source.unsplash.com/random/800x800/?img=1"
urls = [url] * 50


def download_image(url):
    timestamp = time.time()
    response = requests.get(url)
    open(f"images/file_{timestamp}.jpg", "wb").write(response.content)

start = time.perf_counter()

with ThreadPoolExecutor(max_workers=8) as executor:
    executor.map(download_image, urls)

finish = time.perf_counter()

print(f"It took {finish-start}")