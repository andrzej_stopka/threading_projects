import requests
from concurrent.futures import ThreadPoolExecutor
from threading import Lock
import time


url = "https://source.unsplash.com/random/800x800/?img=1"
urls = [url] * 50

file_counter = 1
counter_lock = Lock()

def download_image(url):
    global file_counter
    response = requests.get(url)
    counter_lock.acquire()
    open(f"images/file{file_counter}.jpg", "wb").write(response.content)
    file_counter += 1
    print(f"Downloaded {file_counter}")
    counter_lock.release()

start = time.perf_counter()

with ThreadPoolExecutor(max_workers=8) as executor:
    executor.map(download_image, urls)

finish = time.perf_counter()

print(f"It took {finish-start}")


